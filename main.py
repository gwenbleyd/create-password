import random
import string
import asyncio
import logging
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.types import InputFile
from aiogram.dispatcher.filters.state import State, StatesGroup
from PIL import Image
from pdf2image import convert_from_path
from io import BytesIO
import config

logger = logging.getLogger(__name__)


class PasswordState(StatesGroup):
    length = State()


class JpegToPdfState(StatesGroup):
    file = State()


class PdfToJpegState(StatesGroup):
    file = State()


bot = Bot(token=config.BOT_TOKEN, parse_mode="HTML")
dp = Dispatcher(bot, storage=MemoryStorage())


@dp.message_handler(commands=["start"])
async def start(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    buttons = ["/password", "/jpegtopd", "/pdftojpeg"]
    keyboard.add(*buttons)
    await message.answer("Привет, я телеграм бот, который может:\n"
                         "- Генерировать пароль\n"
                         "- Конвертировать файл из jpeg в pdf\n"
                         "- Конвертировать файл из pdf в jpeg\n"
                         "Чтобы начать, выберите одну из этих функций или введите /help для получения справки.",
                         reply_markup=keyboard)


@dp.message_handler(commands=["help"])
async def help(message: types.Message):
    await message.answer("Вот как вы можете использовать мои функции:\n"
                         "- Генерировать пароль: введите /password и следуйте инструкциям.\n"
                         "- Конвертировать файл из jpeg в pdf: введите /jpegtopdf и прикрепите файл в формате jpeg.\n"
                         "- Конвертировать файл из pdf в jpeg: введите /pdftojpeg и прикрепите файл в формате pdf.\n"
                         "Если вы хотите отменить текущую операцию, введите /cancel.")


@dp.message_handler(commands=["password"], state="*")
async def password(message: types.Message, state: FSMContext):
    await state.finish()
    await PasswordState.length.set()
    await message.answer("Введите желаемую длину пароля (целое число от 8 до 32):")


@dp.message_handler(state=PasswordState.length)
async def password_length(message: types.Message, state: FSMContext):
    length = message.text
    try:
        length = int(length)
        if not 8 <= length <= 32:
            raise ValueError()
    except ValueError:
        await message.answer("Неверная длина пароля. Пожалуйста, введите целое число от 8 до 32:")
        logger.error(f"Пользователь {message.from_user.id} отправил неверную длину пароля")
        return
    chars = string.ascii_letters + string.digits + string.punctuation
    password = "".join(random.choice(chars) for _ in range(length))
    await message.answer(f"Ваш пароль: {password}")
    logger.info(f"Пользователь {message.from_user.id} получил пароль")
    await state.finish()


@dp.message_handler(commands=["jpegtopdf"], state="*")
async def jpeg_to_pdf(message: types.Message, state: FSMContext):
    await state.finish()
    await JpegToPdfState.file.set()
    await message.answer("Прикрепите файл в формате jpeg, который вы хотите конвертировать в pdf:")


@dp.message_handler(content_types=["photo"], state=JpegToPdfState.file)
async def jpeg_to_pdf_file(message: types.Message, state: FSMContext):
    await message.answer("Скачиваю файл...")
    photo = await bot.get_file(message.photo[-1].file_id)
    if not photo.file_path.lower().endswith((".jpeg", ".jpg")):
        await message.answer("Неверный формат файла. Пожалуйста, прикрепите файл в формате jpeg:")
        logger.error(f"Пользователь {message.from_user.id} отправил неверный файл в pdf_to_jpeg")
        return
    photo_bytes = await bot.download_file(photo.file_path)
    image = Image.open(photo_bytes)
    pdf_bytes = BytesIO()
    image.save(pdf_bytes, "PDF")
    pdf_bytes.seek(0)
    await message.answer_document(pdf_bytes, caption="Ваша фотография в формате PDF")
    logger.info(f"Пользователь {message.from_user.id} конвертировал jpeg в pdf")

    await state.finish()


@dp.message_handler(commands=["pdftojpeg"], state="*")
async def pdf_to_jpeg(message: types.Message, state: FSMContext):
    await state.finish()
    await PdfToJpegState.file.set()
    await message.answer("Прикрепите файл в формате pdf, который вы хотите конвертировать в jpeg:")


@dp.message_handler(content_types=["document"], state=PdfToJpegState.file)
async def pdf_to_jpeg_file(message: types.Message, state: FSMContext):
    file = await bot.get_file(message.document.file_id)
    file_name = message.document.file_name

    if not file_name.lower().endswith(".pdf"):
        await message.reply("Это не PDF-файл. Пожалуйста, отправь мне PDF-файл.")
        logger.error(f"Пользователь {message.from_user.id} отправил неверный файл в pdf_to_jpeg")
        return

    await message.answer("Скачиваю файл...")
    await bot.download_file(file.file_path, file_name)
    print(file.file_path)

    await message.answer("Конвертирую файл в изображения...")
    images = convert_from_path(file_name, 300, poppler_path=r'D:\\Library\\bin')

    await message.answer("Отправляю изображения...")

    for image in images:
        buffer = BytesIO()
        image.save(buffer, format='JPEG')
        buffer.seek(0)
        file = InputFile(buffer)
        await bot.send_photo(chat_id=message.from_user.id, photo=file)
    logger.info(f"Пользователь {message.from_user.id} конвертировал pdf-файл в jpeg")
    await state.finish()


@dp.message_handler(commands=["cancel"], state="*")
async def cancel(message: types.Message, state: FSMContext):
    await state.finish()
    await message.answer("Действие отменено")
    logger.info(f"Пользователь {message.from_user.id} отменил действие")


async def main():
    logging.basicConfig(level=logging.INFO,
                        filename="logger.log",
                        format="%(asctime)s - %(module)s - %(levelname)s - %(funcName)s: %(lineno)d - %(message)s",
                        datefmt='%H:%M:%S',
                        )
    logger.info("Starting bot")
    await dp.start_polling()

if __name__ == '__main__':
    asyncio.run(main())
